package com.ascrossgams.myapplication.data.datasource;

import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.ascrossgams.myapplication.StartActivity;
import com.ascrossgams.myapplication.data.Repository;
import com.ascrossgams.myapplication.data.models.User;
import com.ascrossgams.myapplication.ui.Entrance.Registration.FragmentRegistration1;
import com.ascrossgams.myapplication.ui.Entrance.SignIn.FragmentSingIn;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.Map;

import androidx.annotation.NonNull;

import static android.content.ContentValues.TAG;

public class FirebaseDataSource {

    public String mail;
    public String password;
    public String name;
    public String surname;
    public String birthDate;
    public String phone;
    public String country;
    public String city;
    public String id;

    public String getId() {
        return id;
    }

    public String getMail() {
        return mail;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    private static FirebaseDataSource INSTANCE;
    public         FirebaseFirestore  db;

    public static FirebaseDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FirebaseDataSource();
        }

        return INSTANCE;
    }

    public FirebaseDataSource() {
        db = FirebaseFirestore.getInstance();
    }

    public void createUser(Map<String, Object> userData) {

//         Add a new document with a generated ID
        db.collection("users")
                .add(userData)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                       // FragmentRegistration1.mPreferencesDataSource.setStringValue("KEY_USER_ID", documentReference.getId());

                        Log.d("TAG", "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TAG", "Error adding document", e);
                    }
                });
    }

    public void updateUser(final Map<String, Object> userData) {

//        if (getName() != null) {
//
//        }

        db.collection("users")
                .add(userData)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        // FragmentRegistration1.mPreferencesDataSource.setStringValue("KEY_USER_ID", documentReference.getId());

                        id = documentReference.getId();
                        Log.d("TAG", "DocumentSnapshot added with ID: " + documentReference.getId());
                        Log.i(TAG, "LOGGGGGGGGGGG: " + id);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TAG", "Error adding document", e);
                    }
                });

//        String s = userData.get("id").toString();
//        String h =  userData.get("id").toString();
//        //String id = userData.get("id").toString();
//        String id = "testID";
//        if (userData.get("mail") != null) {
//            id = userData.get("mail").toString();
//        }
////        else {
////            return;
////        }
//
//        db.collection("users")
//                .document(id).update(userData)
//                .addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        boolean b = true;
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.w("TAG", "Error adding document", e);
//                    }
//                });
    }

//    public void readUserForPrifile(String mail, String password, String name, String surname, String birthDate, String phone, String country, String city) {
//
//    }


    public void readUser(final String mailLogin, final String passwordLogin) {
        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (mailLogin.equals(document.getString("mail")) && passwordLogin.equals(document.getString("password"))) {
//                                    SharedPreferences sharedPreferences = context.getSharedPreferences("KEY_USER", 0);
//                                    final Gson gson = new Gson();
//                                    gson.fromJson(sharedPreferences.getString("KEY_USER", ""), (Type) User.class);
//                                    String json = sharedPreferences.getString("KEY_USER", "");

                                    mail = document.getString("mail");
                                    password = document.getString("password");
                                    name = document.getString("name");
                                    surname = document.getString("surname");
                                    phone = document.getString("phoneNumber");
                                    birthDate = document.getString("birthDate");
                                    country = document.getString("country");
                                    city = document.getString("city");

                                    id = document.getId();
                                    //id = document.getString("id");
                                }

                                else {
                                    Log.d("Wrong", "Wrong mail or password");
                                }
                                Log.d("TAG", document.getId() + " => " + document.getData());
                            }


                        } else {
                            Log.w("TAG", "Error getting documents. ", task.getException());
                        }
                    }
                });
    }


    public void updateUserFromProfile(final String mailLogin, final Map<String, Object> userData) {
        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (mailLogin.equals(document.getString("mail"))) {
//                                    SharedPreferences sharedPreferences = context.getSharedPreferences("KEY_USER", 0);
//                                    final Gson gson = new Gson();
//                                    gson.fromJson(sharedPreferences.getString("KEY_USER", ""), (Type) User.class);
//                                    String json = sharedPreferences.getString("KEY_USER", "");

                                    mail = document.getString("mail");
                                    password = document.getString("password");
                                    name = document.getString("name");
                                    surname = document.getString("surname");
                                    phone = document.getString("phoneNumber");
                                    birthDate = document.getString("birthDate");
                                    country = document.getString("country");
                                    city = document.getString("city");

                                    id = document.getId();
                                    //id = document.getString("id");
                                }
                                Log.d("TAG", document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.w("TAG", "Error getting documents. ", task.getException());
                        }
                    }
                });

        db.collection("users")
                .document(id).update(userData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        boolean b = true;
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TAG", "Error adding document", e);
                    }
                });
    }



}