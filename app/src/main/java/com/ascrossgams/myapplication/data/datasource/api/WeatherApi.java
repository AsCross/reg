package com.ascrossgams.myapplication.data.datasource.api;

import android.content.res.Resources;

import com.ascrossgams.myapplication.data.datasource.PreferencesDataSource;
import com.ascrossgams.myapplication.data.models.retrofit.RetroLocation;
import com.ascrossgams.myapplication.data.models.retrofit.RetroWeatherResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherApi {
//    PreferencesDataSource mPref = new PreferencesDataSource(getCon);

//RetroLocation currentLocation = locationrList.get(position);

    // String city = "Kiev";

    String API_KEY = "a93c65012f3362c5374a5dc7528a8c89";
    //http://api.weatherstack.com/current?access_key=a93c65012f3362c5374a5dc7528a8c89&query=Kiev&language=ru

//    @GET("current?access_key=" + API_KEY + "&query=" + city)
//    Call<RetroWeatherResponseModel> getWeatherInfo();

    @GET("current?access_key=" + API_KEY)
    Call<RetroWeatherResponseModel> getWeatherInfoByCity(@Query("query") String city);

//
//    @GET("api.php?company_name={name}")
//    Call<Model> getRoms_center(@Query("name") String name);
//
//    @GET("api.php")
//    Call<Model> getRoms_center(@Query("company_name") String name);
}