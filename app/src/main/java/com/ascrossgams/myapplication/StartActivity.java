package com.ascrossgams.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.ascrossgams.myapplication.data.datasource.PreferencesDataSource;
import com.ascrossgams.myapplication.ui.Entrance.Registration.FragmentRegistration1;
import com.ascrossgams.myapplication.ui.Entrance.Registration.FragmentRegistration2;
import com.ascrossgams.myapplication.ui.Entrance.SignIn.FragmentSingIn;
import com.google.android.material.navigation.NavigationView;

public class StartActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    FragmentSingIn singInFrag;
    FragmentRegistration1 registration1Frag;
    FragmentRegistration2 registration2Frag;

    Button reg1;
    Button signIn;

    FragmentTransaction fTrans;
    CheckBox chbStack;
    String isSignIn;

    PreferencesDataSource mPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        reg1 = findViewById(R.id.go_to_registration);
        signIn = findViewById(R.id.go_to_sing_in);
        singInFrag = new FragmentSingIn();
        registration1Frag = new FragmentRegistration1();
        registration2Frag = new FragmentRegistration2();
        fTrans = getSupportFragmentManager().beginTransaction();

        mPref = new PreferencesDataSource(getBaseContext());
        //mPref.setStringValue("KEY_EXIT", "no");



        //isSignIn = "no";



//        else if (mPref.getStringValue("KEY_EXIT", "").equals("yes")) {
//            mPref.setStringValue("KEY_EXIT", "no");
//        }

        if(mPref.getStringValue("KEY_EXIT", "").equals("no") || mPref.getStringValue("KEY_EXIT", "").equals("")) {
            mPref.setStringValue("KEY_EXIT", "yes");
        }


        else if (mPref.getStringValue("KEY_EXIT", "").equals("yes")) {
            //isSignIn = getIntent().getStringExtra("isSignIn");
            mPref.setStringValue("KEY_EXIT", "no");
            fTrans.replace(R.id.frgmCont, singInFrag);
            reg1.setVisibility(View.GONE);
            signIn.setVisibility(View.GONE);
            fTrans.commit();
    }






        //chbStack = (CheckBox)findViewById(R.id.chbStack);
    }

    public void toMainActivity(View view) {
        Intent questionIntent = new Intent(StartActivity.this,
                MainActivity.class);
        startActivityForResult(questionIntent, 0);
        //overridePendingTransition(R.anim.alpha_on, R.anim.alpha_off);
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.go_to_registration:
                fTrans.replace(R.id.frgmCont, registration1Frag);
                reg1.setVisibility(View.GONE);
                signIn.setVisibility(View.GONE);
                break;
            case R.id.go_to_sing_in:
                fTrans.replace(R.id.frgmCont, singInFrag);
                reg1.setVisibility(View.GONE);
                signIn.setVisibility(View.GONE);
                break;
            case R.id.registration_1:
                fTrans.replace(R.id.frgmCont, registration2Frag);
                break;

//            case R.id.btnReplace:
//                fTrans.replace(R.id.frgmCont, frag2);
            default:
                break;
        }
        //if (chbStack.isChecked()) fTrans.addToBackStack(null);
        fTrans.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}