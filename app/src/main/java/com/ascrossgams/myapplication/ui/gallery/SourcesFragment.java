package com.ascrossgams.myapplication.ui.gallery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ascrossgams.myapplication.R;
import com.ascrossgams.myapplication.data.models.retrofit.RetroSources;
import com.ascrossgams.myapplication.ui.source.SourceFragment;

import java.util.List;

public class SourcesFragment extends Fragment {

    private SourcesViewModel sourcesViewModel;
    private RecyclerView recyclerView;
    SourceFragment sourceFragment;

    //SoursesAdapter

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        sourcesViewModel =
                ViewModelProviders.of(this).get(SourcesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);

        recyclerView = root.findViewById(R.id.recyclerViewSources);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        sourceFragment = new SourceFragment();

        sourcesViewModel.getSourcesLiveData().observe(this, new Observer<List<RetroSources>>() {
            @Override
            public void onChanged(List<RetroSources> retroSources) {
                recyclerView.setAdapter(new SoursesAdapter(retroSources, getContext()));
            }
        });

        sourcesViewModel.getErrorsLiveData().observe(this, new Observer<Throwable>() {
            @Override
            public void onChanged(Throwable throwable) {
                Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        sourcesViewModel.loadSources();

//        sourcesViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });


        return root;
    }
}