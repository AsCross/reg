package com.ascrossgams.myapplication.ui.share;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.ascrossgams.myapplication.MainActivity;
import com.ascrossgams.myapplication.R;
import com.ascrossgams.myapplication.StartActivity;

public class ExitFragment extends Fragment {

    private ExitViewModel shareViewModel;
    String isSignIn;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        isSignIn = "yes";
        Intent intent = new Intent(getContext(), StartActivity.class);
        intent.putExtra("isSignIn", isSignIn);
        startActivity(intent);
        shareViewModel =
                ViewModelProviders.of(this).get(ExitViewModel.class);
        View root = inflater.inflate(R.layout.fragment_share, container, false);
        final TextView textView = root.findViewById(R.id.text_share);
        shareViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}