package com.ascrossgams.myapplication.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ascrossgams.myapplication.R;
import com.ascrossgams.myapplication.data.Repository;
import com.ascrossgams.myapplication.data.models.User;
import com.ascrossgams.myapplication.data.models.retrofit.RetroArticle;
import com.ascrossgams.myapplication.data.models.retrofit.RetroArticlesResponseModel;
import com.ascrossgams.myapplication.ui.article.ArticleFragment;

import java.util.List;

public class NewsFragment extends Fragment {

    ArticleFragment articleFragment;
    private NewsViewModel newsViewModel;
    private RecyclerView recyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        newsViewModel =
                ViewModelProviders.of(this).get(NewsViewModel.class);

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        articleFragment = new ArticleFragment();

        newsViewModel.getArticlesLiveData().observe(this, new Observer<List<RetroArticle>>() {
            @Override
            public void onChanged(@Nullable List<RetroArticle> articles) {
                recyclerView.setAdapter(new NewsAdapter(articles, getContext()));
            }
        });

        newsViewModel.getErrorsLiveData().observe(this, new Observer<Throwable>() {
            @Override
            public void onChanged(Throwable throwable) {
                Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        newsViewModel.loadArticles();

        return root;
    }
}