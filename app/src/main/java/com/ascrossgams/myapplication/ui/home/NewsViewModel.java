package com.ascrossgams.myapplication.ui.home;

import android.widget.Toast;

import com.ascrossgams.myapplication.data.Repository;
import com.ascrossgams.myapplication.data.models.retrofit.RetroArticle;
import com.ascrossgams.myapplication.data.models.retrofit.RetroArticlesResponseModel;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsViewModel extends ViewModel {

    private MutableLiveData<List<RetroArticle>> articlesLiveData;
    private MutableLiveData<Throwable> errorsLiveData;

    public NewsViewModel() {
        articlesLiveData = new MutableLiveData<>();
        errorsLiveData = new MutableLiveData<>();
    }


    public MutableLiveData<List<RetroArticle>> getArticlesLiveData() {
        return articlesLiveData;
    }

    public MutableLiveData<Throwable> getErrorsLiveData() {
        return errorsLiveData;
    }

    public void loadArticles() {

        Repository.getInstance().getAllArticles(new Callback<RetroArticlesResponseModel>() {
            @Override
            public void onResponse(Call<RetroArticlesResponseModel> call, Response<RetroArticlesResponseModel> response) {
                articlesLiveData.postValue(response.body().getArticles());
            }

            @Override
            public void onFailure(Call<RetroArticlesResponseModel> call, Throwable t) {
                errorsLiveData.postValue(t);
            }
        });
    }


}