package com.ascrossgams.myapplication.ui.gallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ascrossgams.myapplication.R;
import com.ascrossgams.myapplication.data.models.retrofit.RetroArticle;
import com.ascrossgams.myapplication.data.models.retrofit.RetroSources;
import com.ascrossgams.myapplication.ui.article.ArticleFragment;
import com.ascrossgams.myapplication.ui.home.NewsAdapter;

import java.util.List;

public class SoursesAdapter extends RecyclerView.Adapter<SoursesAdapter.SourcesAdapterViewHolder>  {

    private List<RetroSources> sourcesList;
    private Context context;
    SourcesFragment sourcesFragment;

    public SoursesAdapter(List<RetroSources> sourcesList, Context context) {
        this.sourcesList = sourcesList;
        this.context = context;
    }

    @NonNull
    @Override
    public SourcesAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_source, parent, false);
        return new SourcesAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SourcesAdapterViewHolder holder, int position) {
        RetroSources currentSource = sourcesList.get(position);
        holder.description.setText(currentSource.getDescription());
        holder.name.setText(currentSource.getName());
    }

    @Override
    public int getItemCount() {
        return sourcesList.size();
    }

    class SourcesAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView description;


        public SourcesAdapterViewHolder(@NonNull final View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sourcesFragment = new SourcesFragment();



                    Toast.makeText(itemView.getContext(), "Go to source",
                            Toast.LENGTH_LONG).show();




                    //signIn();
//                String id = user.getId().toString();
//                viewModelRegistration1.proceedSignIn(user, user.getId().toString());


                }
            });
            name = itemView.findViewById(R.id.sourceName);
            description = itemView.findViewById(R.id.sourceDescription);
        }
    }
}
