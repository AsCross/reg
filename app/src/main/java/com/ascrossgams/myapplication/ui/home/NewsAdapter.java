package com.ascrossgams.myapplication.ui.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ascrossgams.myapplication.R;
import com.ascrossgams.myapplication.data.models.User;
import com.ascrossgams.myapplication.data.models.retrofit.RetroArticle;
import com.ascrossgams.myapplication.ui.article.ArticleFragment;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsAdapterViewHolder> {

    private List<RetroArticle> articleList;
    private Context context;
    ArticleFragment articleFragment;

    public NewsAdapter(List<RetroArticle> articleList, Context context) {
        this.articleList = articleList;
        this.context = context;
    }

    @NonNull
    @Override
    public NewsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_article, parent, false);
        return new NewsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapterViewHolder holder, int position) {
        RetroArticle currentArticle = articleList.get(position);
//        holder.articleText.setText(currentArticle.getDescription());
        holder.titleText.setText(currentArticle.getTitle());

    //    Picasso.with(context).load(currentArticle.getUrlToImage()).into(holder.thumbnailImage);
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    class NewsAdapterViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnailImage;
        TextView titleText;
        TextView articleText;


        public NewsAdapterViewHolder(@NonNull final View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        articleFragment = new ArticleFragment();



                        Toast.makeText(itemView.getContext(), "Go to article",
                                Toast.LENGTH_LONG).show();




                    //signIn();
//                String id = user.getId().toString();
//                viewModelRegistration1.proceedSignIn(user, user.getId().toString());


                }
            });
            //thumbnailImage = itemView.findViewById(R.id.thumbnailImage);
            titleText = itemView.findViewById(R.id.titleText);
            //articleText = itemView.findViewById(R.id.articleText);
        }
    }
}
