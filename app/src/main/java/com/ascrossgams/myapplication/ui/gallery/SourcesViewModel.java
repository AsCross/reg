package com.ascrossgams.myapplication.ui.gallery;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ascrossgams.myapplication.data.Repository;
import com.ascrossgams.myapplication.data.models.retrofit.RetroAllSourceResponseModel;
import com.ascrossgams.myapplication.data.models.retrofit.RetroSources;

import java.util.List;

import io.opencensus.resource.Resource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SourcesViewModel extends ViewModel {

    private MutableLiveData<List<RetroSources>> sourcesLiveData;
    private MutableLiveData<Throwable> errorsLiveData;

    public SourcesViewModel() {
        sourcesLiveData = new MutableLiveData<>();
        errorsLiveData = new MutableLiveData<>();
    }


    public MutableLiveData<List<RetroSources>> getSourcesLiveData() {
        return sourcesLiveData;
    }

    public MutableLiveData<Throwable> getErrorsLiveData() {
        return errorsLiveData;
    }

    public void loadSources() {

        Repository.getInstance().getAllSources(new Callback<RetroAllSourceResponseModel>() {
            @Override
            public void onResponse(Call<RetroAllSourceResponseModel> call, Response<RetroAllSourceResponseModel> response) {
                //Response s = response;
                sourcesLiveData.postValue(response.body().getSources());
            }

            @Override
            public void onFailure(Call<RetroAllSourceResponseModel> call, Throwable t) {
                errorsLiveData.postValue(t);
            }
        });
    }

}